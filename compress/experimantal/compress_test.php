<?php

function getPassword($str, $total_len){
  echo "$str\n";
  $total_len = 128;
  $hex_array = array('0','1','2','3','4','5','6','7','8','9','a', 'b', 'c', 'd', 'e', 'f');

  $characters = "0123456789abcdef";
  $char_length = strlen($characters);
  $str_length = strlen($str);

  if($str_length >= $total_len) return 0;

  for($i=0; $i<$str_length; $i++){
    $break_loop = 0;
    for($l=0; $l<$char_length;$l++){
      if($l < ($char_length - 1)){
        if($str[$i] == $characters[$l]){$str[$i] = $characters[$l+1];$break_loop = 1;break;}
      }
      if($l == ($char_length - 1)){
        if($i > 0){
          for($k=0; $k<=($i); $k++){
            $str[$k] = $characters[0];
          }
        }else{
          $str[0] = $characters[0];
        }
      }
    }
    if($break_loop == 1){
      break;
    }
  }
  if($i == $str_length){
    for($j=0; $j<$str_length; $j++){
      $str[$j] = $characters[0];
    }
    $str[$i] = $characters[0];
    //$str[$i+1] = NULL;
  }
  return $str;
}

$str = "000000000000000";
while(1){
  $str = getPassword($str,3);
  if(!strcmp($str,"0000000000000000")){
    break;
  }
}
?>
